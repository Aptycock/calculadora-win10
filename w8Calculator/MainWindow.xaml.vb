﻿Class MainWindow
    WithEvents evaluator As New Evaluator
    Private expresion As String = ""
    Private strNum As String = ""
    Private cadena As String = ""

    Private Sub Button_Click(sender As Object, e As RoutedEventArgs)

        cadena = CType(sender, Button).Content
        If Not (cadena.Equals("=") Or cadena.Equals("CE") Or cadena.Equals("C") Or cadena.Equals("⌫")) Then
            expresion = expresion + cadena
        End If

        If Not (cadena.Equals("=") Or cadena.Equals("CE") Or cadena.Equals("C") Or cadena.Equals("⌫") Or
            cadena.Equals("+") Or cadena.Equals("-") Or cadena.Equals("*") Or cadena.Equals("/")) Then
            strNum = strNum + cadena
        End If

        Resultado.Text = strNum
        Select Case (CType(sender, Button).Content)
            Case "M"
            Case "CE"
                Resultado.Text = 0
            Case "C"
                expresion = ""
                strNum = ""
                Resultado.Text = 0
                Historial.Text = ""
                Try
                    expresion = expresion.Remove(expresion.Length - 1)
                    strNum = strNum.Remove(strNum.Length - 1)
                    Resultado.Text = Resultado.Text.Remove(Resultado.Text.Length - 1)
                Catch ex As ArgumentOutOfRangeException

                End Try
            Case "+"
                strNum = ""
                Historial.Text = expresion
            Case "-"
                strNum = ""
                Historial.Text = expresion
            Case "*"
                strNum = ""
                Historial.Text = expresion
            Case "/"
                strNum = ""
                Historial.Text = expresion
            Case "±"
            Case "1"
                Resultado.Text = strNum
            Case "2"
                Resultado.Text = strNum
            Case "3"
                Resultado.Text = strNum
            Case "4"
                Resultado.Text = strNum
            Case "5"
                Resultado.Text = strNum
            Case "6"
                Resultado.Text = strNum
            Case "7"
                Resultado.Text = strNum
            Case "8"
                Resultado.Text = strNum
            Case "9"
                Resultado.Text = strNum
            Case "0"
                Resultado.Text = strNum
            Case "="
                Historial.Text = ""
                Resultado.Text = Operate(expresion)
                'expresion = Resultado.Text
                'expresion = expresion + "+" + strNum
        End Select
    End Sub

    Function Operate(ByRef input As String) As Double
        Try
            Return CDec(evaluator.evaluar(input).ToString)
        Catch ex As NullReferenceException
            Return 0
        End Try
    End Function

    Sub cross(ByVal input As String)
        MsgBox((Mid(expresion, 1, Len(expresion) - 1)))
        evaluator.evaluar(Mid(expresion, 1, Len(expresion) - 1))
    End Sub

End Class

Public Class Evaluator
    Private mParser As New parser(Me)

    Enum Prioridad
        none = 0
        masmenos = 1
        muldiv = 2
        menos = 3
    End Enum

    Enum Tipo
        nada
        finFormula
        signoMas
        signoMenos
        signoMultiplicacion
        singoDivicion
        abrirParentesis
        cerrarParentesis
        valorNumero
    End Enum

    Private Class Identificador
        Private mString, mLongitud As String
        Private mPos, posicioninicial As Integer
        Private mActChar As Char
        Public tipo As Tipo
        Public valor As New System.Text.StringBuilder
        Private mParser As parser

        Sub New(ByVal Parser As parser, ByVal str As String)
            mString = str
            mLongitud = str.Length
            mPos = 0
            mParser = Parser
            siguienteCaracter()
        End Sub

        Sub siguienteCaracter()
            If mPos < mLongitud Then
                mActChar = mString.Chars(mPos)
                mPos += 1
            Else
                mActChar = Nothing
            End If
        End Sub

        Public Function IsOp() As Boolean
            Return mActChar = "+"c Or mActChar = "-"c Or mActChar = "%"c _
            Or mActChar = "/"c Or mActChar = "("c Or mActChar = ")"c
        End Function

        Public Sub Siguiente()
            posicioninicial = mPos
            valor.Length = 0
            tipo = Tipo.nada
            Do
                Select Case mActChar
                    Case Nothing
                        tipo = Tipo.finFormula
                    Case "0"c To "9"c
                        ParseNumero()
                    Case "-"c
                        siguienteCaracter()
                        tipo = Tipo.signoMenos
                    Case "+"c
                        siguienteCaracter()
                        tipo = Tipo.signoMas
                    Case "*"c
                        siguienteCaracter()
                        tipo = Tipo.signoMultiplicacion
                    Case "/"c
                        siguienteCaracter()
                        tipo = Tipo.singoDivicion
                    Case "("c
                        siguienteCaracter()
                        tipo = Tipo.abrirParentesis
                    Case ")"c
                        siguienteCaracter()
                        tipo = Tipo.cerrarParentesis
                    Case Chr(0) To " "c

                    Case Else
                        ParseIdentifier()
                End Select
                If tipo <> Tipo.nada Then Exit Do
                siguienteCaracter()
            Loop
        End Sub

        Public Sub ParseNumero()
            tipo = Tipo.valorNumero
            While mActChar >= "0"c And mActChar <= "9"c
                valor.Append(mActChar)
                siguienteCaracter()
            End While
            If mActChar = "."c Then
                valor.Append(mActChar)
                siguienteCaracter()
                While mActChar >= "0"c And mActChar <= "9"c
                    valor.Append(mActChar)
                    siguienteCaracter()
                End While
            End If
        End Sub

        Public Sub ParseIdentifier()
            While (mActChar >= "0"c And mActChar <= "9"c) _
               Or (mActChar >= "a"c And mActChar <= "z"c) _
               Or (mActChar >= "A"c And mActChar <= "Z"c) _
               Or (mActChar >= "A"c And mActChar <= "Z"c) _
               Or (mActChar >= Chr(128)) _
               Or (mActChar = "_"c)
                valor.Append(mActChar)
                siguienteCaracter()
            End While
        End Sub

        Public Sub ParseString(ByVal InQuote As Boolean)
            Dim OriginalChar As Char
            If InQuote Then
                OriginalChar = mActChar
                siguienteCaracter()
            End If

            Do While mActChar <> Nothing
                If InQuote AndAlso mActChar = OriginalChar Then
                    siguienteCaracter()
                    If mActChar = OriginalChar Then
                        valor.Append(mActChar)
                        Exit Sub
                    End If
                Else
                    valor.Append(mActChar)
                    siguienteCaracter()
                End If
            Loop
        End Sub

    End Class

    Private Class parser
        Dim identificador As Identificador
        Private mEvaluator As Evaluator

        Sub New(ByVal evaluator As Evaluator)
            mEvaluator = evaluator
        End Sub

        Friend Function ParseExpresion(ByVal Acc As Object, ByVal prioridad As Prioridad) As Object
            Dim valorIzquierda, valorDerecha As Object
            Do
                Select Case identificador.tipo
                    Case Tipo.signoMenos
                        identificador.Siguiente()
                        valorIzquierda = ParseExpresion(0, Prioridad.menos)
                        If TypeOf valorIzquierda Is Double Then
                            valorIzquierda = -DirectCast(valorIzquierda, Double)
                        End If
                        Exit Do
                    Case Tipo.signoMas
                        identificador.Siguiente()
                        Exit Do
                    Case Tipo.valorNumero
                        valorIzquierda = Double.Parse(identificador.valor.ToString)
                        identificador.Siguiente()
                        Exit Do
                    Case Tipo.abrirParentesis
                        identificador.Siguiente()
                        valorIzquierda = ParseExpresion(0, Prioridad.none)
                        If identificador.tipo = Tipo.cerrarParentesis Then
                            identificador.Siguiente()
                            Exit Do
                        End If
                    Case Else
                        Exit Do
                End Select
            Loop
            Do
                Dim tipoOperacion As Tipo
                tipoOperacion = identificador.tipo
                Select Case tipoOperacion
                    Case Tipo.finFormula
                        Return valorIzquierda
                    Case Tipo.valorNumero
                        Exit Function
                    Case Tipo.signoMas
                        If prioridad < Prioridad.masmenos Then
                            identificador.Siguiente()
                            valorDerecha = ParseExpresion(valorIzquierda, Prioridad.masmenos)
                            If TypeOf valorIzquierda Is Double _
                               And TypeOf valorDerecha Is Double Then
                                valorIzquierda = CDbl(valorIzquierda) + CDbl(valorDerecha)
                            Else
                                valorIzquierda = valorIzquierda.ToString & valorDerecha.ToString
                            End If
                        Else
                            Exit Do
                        End If
                    Case Tipo.signoMenos
                        If prioridad < Prioridad.masmenos Then
                            identificador.Siguiente()
                            valorDerecha = ParseExpresion(valorIzquierda, Prioridad.masmenos)
                            If TypeOf valorIzquierda Is Double _
                               And TypeOf valorDerecha Is Double Then
                                valorIzquierda = CDbl(valorIzquierda) - CDbl(valorDerecha)
                            End If
                        Else
                            Exit Do
                        End If
                    Case Tipo.signoMultiplicacion, Tipo.singoDivicion
                        If prioridad < Prioridad.muldiv Then
                            identificador.Siguiente()
                            valorDerecha = ParseExpresion(valorIzquierda, Prioridad.muldiv)

                            If TypeOf valorIzquierda Is Double _
                               And TypeOf valorDerecha Is Double Then
                                If tipoOperacion = Tipo.signoMultiplicacion Then
                                    valorIzquierda = CDbl(valorIzquierda) * CDbl(valorDerecha)
                                Else
                                    valorIzquierda = CDbl(valorIzquierda) / CDbl(valorDerecha)
                                End If
                            End If
                        Else
                            Exit Do
                        End If
                    Case Else

                        Exit Do
                End Select
            Loop

            Return valorIzquierda
        End Function

        Private Function getVariableInterna() As Object
            Dim parametros As New ArrayList
            Dim valorIzquierda As Object
            Dim objetoActual As Object
            Do
                Dim func As String = identificador.valor.ToString
                identificador.Siguiente()
                parametros.Clear()
                If identificador.tipo = Tipo.abrirParentesis Then
                    identificador.Siguiente()
                    Do
                        If identificador.tipo = Tipo.cerrarParentesis Then
                            identificador.Siguiente()
                            Exit Do
                        End If
                        valorIzquierda = ParseExpresion(0, Prioridad.none)
                        parametros.Add(valorIzquierda)
                        If identificador.tipo = Tipo.cerrarParentesis Then
                            identificador.Siguiente()
                            Exit Do
                        End If
                    Loop
                End If
            Loop
            Return valorIzquierda
        End Function

        Public Function Eval(ByVal input As String) As Object
            If Len(input) > 0 Then
                identificador = New Identificador(Me, input)
                identificador.Siguiente()
                Dim res As Object = ParseExpresion(Nothing, Prioridad.none)
                If identificador.tipo = Tipo.finFormula Then
                    Return res
                End If
            End If
        End Function

    End Class

    Public Function EvalDouble(ByRef formula As String) As Double
        Dim res As Object = evaluar(formula)
        If TypeOf res Is Double Then
            Return CDbl(res)
        End If
    End Function

    Public Function evaluar(ByVal input As String) As Object
        Return mParser.Eval(input)
    End Function
    Event GetVariable(ByVal name As String, ByRef value As Object)

End Class


